﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Nancy;

namespace TrollTickets
{
    public class IndexModule : NancyModule
    {
        public IndexModule(IRootPathProvider path)
        {
            var random = new Random((int) DateTime.Now.Ticks);
            Get["/"] = _ =>
            {
                var tickets =
                    File.ReadAllText(Path.Combine(path.GetRootPath(), "tickets.txt"))
                        .Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries);
                var next = random.Next(0, tickets.Length);
                return View["Index", tickets[next]];

            };
        }
    }
}